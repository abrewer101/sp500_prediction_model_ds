# **SP500 Prediction Model**

## **Project Overview**
This project aims to predict market trends using historical data from the SPY ETF, which tracks the performance of the S&P 500. The model uses the XGBoost machine learning algorithm to predict the closing price based on open prices and trading volume.

## **Dataset**
The dataset, `SPY.csv`, was downloaded from Yahoo Finance. It contains daily trading information for the SPY ETF, including open, high, low, close prices, and volume.

## **Prerequisites**
- Python 3.x
- pandas
- XGBoost
- matplotlib

Ensure you have the required libraries installed. You can install them using pip:
```bash
pip install pandas xgboost matplotlib
```

## **Usage**
To run this project, follow these steps:

Clone or download the repository to your local machine.
Ensure you have Jupyter Notebook installed or use Jupyter Lab/Google Colab to open the .ipynb file.
Run the notebook cells sequentially to load the data, visualize it, train the model, and view the predictions.

## **Model**
The project utilizes the XGBoost regression model to predict the closing price of the SPY ETF. The features used for the prediction are the opening price and trading volume of the stock.


## **Results**
The model's predictions are plotted against the actual closing prices for visualization. Additionally, the model's accuracy score is printed at the end of the notebook.

## **Conclusion**
This project demonstrates the potential of machine learning in predicting stock market trends. The model shows promising results, but further tuning and additional features could improve its accuracy.

## **Acknowledgements**
Data source: Yahoo Finance
Libraries: pandas, XGBoost, matplotlib
